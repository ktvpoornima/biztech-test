import { Component, OnInit  } from '@angular/core';
import { Options } from 'ng5-slider';

interface SimpleSliderModel {
  value: number;
  options: Options;
}

@Component({
  selector: 'app-aside',
  templateUrl: './aside.component.html',
  styleUrls: ['./aside.component.scss']
})


export class AsideComponent implements OnInit {

  verticalSlider: SimpleSliderModel = {
  value: 16,
  options: {
    floor: 0,
    ceil: 32,
    showSelectionBar: true
  }
}
verticalSlider1: SimpleSliderModel = {
  value: 80,
  options: {
    floor: 0,
    ceil: 100,
    showSelectionBar: true
  }
}
  
  constructor() { }

  ngOnInit() {
  }

}
